#ifndef __H_COMMON__
#define __H_COMMON__

// TODO: run `make PlottingHelper` in the Darwin root directory to install this library
R__ADD_INCLUDE_PATH($DARWIN_BASE/../PlottingHelper)
#include <plottingHelper.h>
#include <plottingHelper.C>
#include <RemoveOverlaps.h>
//R__ADD_INCLUDE_PATH($POOL/PlottingHelper/plottingHelper_C.so)

using namespace PlottingHelper;

static const double inf = numeric_limits<double>::infinity();
static const double eps = numeric_limits<double>::epsilon();

static const vector<int> colors {kBlue, kRed, kGreen+2, kYellow+2, kViolet, kCyan+2, kYellow-9, 
            kPink+6, kOrange+3, kAzure-4, kGray+3, kGreen-6, kOrange+6, kMagenta-1, kTeal-9};

template<typename T, typename ... Args> inline T * Get (TFile * f, Args ... args) { return dynamic_cast<T*>(f->Get(Form(args...))); }
template<typename T                   > inline T * Get (TFile * f, const char * c) { return dynamic_cast<T*>(f->Get(c)); }
template<typename T                   > inline T * Get (TFile * f, TString s) { return dynamic_cast<T*>(f->Get(s)); }
template<typename T, typename ... Args> inline T * Get (TDirectory * d, Args ... args) { return dynamic_cast<T*>(d->Get(Form(args...))); }
template<typename T                   > inline T * Get (TDirectory * d, const char * c) { return dynamic_cast<T*>(d->Get(c)); }
template<typename T                   > inline T * Get (TDirectory * d, TString s) { return dynamic_cast<T*>(d->Get(s)); }
template<typename T, typename ... Args> inline T * Get (Args ... args) { return dynamic_cast<T*>(gDirectory->Get(Form(args...))); }
template<typename T                   > inline T * Get (const char * c) { return dynamic_cast<T*>(gDirectory->Get(c)); }
template<typename T                   > inline T * Get (TString s) { return dynamic_cast<T*>(gDirectory->Get(s)); }

template<typename T, typename ... Args> inline void Put (T * t, TFile * f, Args ... args) { t->SetDirectory(f); t->Write(Form(args...)); }
template<typename T                   > inline void Put (T * t, TFile * f, const char * c) { t->SetDirectory(f); t->Write(c); }
template<typename T                   > inline void Put (T * t, TFile * f, TString s) { t->SetDirectory(f); t->Write(s); }
template<typename T                   > inline void Put (T * t, TFile * f) { t->SetDirectory(f); t->Write(); }
template<typename T, typename ... Args> inline void Put (T * t, TDirectory * d, Args ... args) { t->SetDirectory(d); t->Write(Form(args...)); }
template<typename T                   > inline void Put (T * t, TDirectory * d, const char * c) { t->SetDirectory(d); t->Write(c); }
template<typename T                   > inline void Put (T * t, TDirectory * d, TString s) { t->SetDirectory(d); t->Write(s); }
template<typename T                   > inline void Put (T * t, TDirectory * d) { t->SetDirectory(d); t->Write(); }
template<typename T, typename ... Args> inline void Put (T * t, Args ... args) { t->SetDirectory(gDirectory); t->Write(Form(args...)); }
template<typename T                   > inline void Put (T * t, const char * c) { t->SetDirectory(gDirectory); t->Write(c); }
template<typename T                   > inline void Put (T * t, TString s) { t->SetDirectory(gDirectory); t->Write(s); }
template<typename T                   > inline void Put (T * t) { t->SetDirectory(gDirectory); t->Write(); }

TString rn() { return Form("%d", rand()); }

#undef assert
#define assert(ARG) \
    if(!(ARG)) {\
        cout << "Assertion `" << #ARG << "' at line " << __LINE__ << endl;\
        exit(1);\
    }

#endif
