#ifndef DAS_PLOT
#define DAS_PLOT

static const double MinDefault = 97, MaxDefault = 3450;

static const vector<TString> yBinsDefault {"|y| < 0.5", "0.5 < |y| < 1.0", "1.0 < |y| < 1.5", "1.5 < |y| < 2.0", "2.0 < |y| < 2.5", "2.5 < |y| < 3.0"};
static TString CMSheader = "#bf{CMS} #it{Internal}";
static int R = 8;

template<int nYbins = 5> struct Plot {

    static int kPosN;
    static double shiftY;
    static bool plotLeg, plotHeader;
    static bool logx, logy;
    static int ndivx, ndivy;

    TString Name, Title;
    const double W, H;
    TString Xtitle, Ytitle;

    vector<TLegend *> l;
    float legAlpha;
    TLine * line;

    const double y1, y2;

    const vector<double> x1, x2;

    vector<TString> yBins;

    bool isMConly;
    bool plotLumi;
    bool plotEnergy;

    static double RM, LM;

    void clean (TH1 * h1, int y)
    {
        for (int i = 0; i <= h1->FindBin(x1.at(y-1)-1); ++i) {
            h1->SetBinContent(i,0);
            h1->SetBinError  (i,0);
        }
        for (int i = h1->FindBin(x2.at(y-1)+1); i <= h1->GetNbinsX()+1; ++i) {
            h1->SetBinContent(i,0);
            h1->SetBinError  (i,0);
        }
    }

    void DrawFrame (int y)
    {
        gPad->DrawFrame(x1.at(y-1), y1, x2.at(y-1), y2, "");
        if (Plot<nYbins>::logx) gPad->SetLogx();
        if (Plot<nYbins>::logy) gPad->SetLogy();
    }

    virtual void cd (int) = 0;
    virtual void SetLogy () = 0;

private:
    void InitLine ()
    {
        line->SetLineStyle(kDashed);
        line->SetLineColor(kBlack);
        line->SetLineWidth(1);
    }

public:
    Plot (const char * name, const char * title, double w, double h,
          TString xtitle, TString ytitle, 
          double low, double high, // y axis range
          vector<double> Min = vector<double>(MinDefault,nYbins), // x range
          vector<double> Max = vector<double>(MaxDefault,nYbins), // y range
          vector<TString> YBins = yBinsDefault) :
        Name(name), Title(title), Xtitle(xtitle), Ytitle(ytitle), W(w), H(h),
        l(nYbins,nullptr), legAlpha(0), line(new TLine),
        y1(low), y2(high), x1(Min), x2(Max),
        yBins(YBins), isMConly(false), plotLumi(true), plotEnergy(true)
    {
        cout << __func__ << '\t' << Title << endl;
        assert(yBins.size() >= static_cast<size_t>(nYbins));
        InitLine();
    }

    Plot (const char * name, const char * title, double w, double h,
          TString xtitle, TString ytitle, 
          double low, double high,
          double Min = MinDefault,
          vector<double> Max = vector<double>(MaxDefault,nYbins),
          vector<TString> YBins = yBinsDefault) :
        Name(name), Title(title), Xtitle(xtitle), Ytitle(ytitle), W(w), H(h),
        l(nYbins,nullptr), legAlpha(0), line(new TLine),
        y1(low), y2(high), x1(nYbins,Min), x2(Max),
        yBins(YBins), isMConly(false), plotLumi(true), plotEnergy(true)
    {
        cout << __func__ << '\t' << Plot<nYbins>::Title << endl;
        assert(yBins.size() >= static_cast<size_t>(nYbins));
        InitLine();
    }

    Plot (const char * name, const char * title, double w, double h,
          TString xtitle, TString ytitle, 
          double low, double high,
          double Min = MinDefault,
          double Max = MaxDefault,
          vector<TString> YBins = yBinsDefault) :
        Name(name), Title(title), Xtitle(xtitle), Ytitle(ytitle), W(w), H(h),
        l(nYbins,nullptr), line(new TLine),
        y1(low), y2(high), x1(nYbins,Min), x2(nYbins,Max),
        yBins(YBins), isMConly(false), plotLumi(true), plotEnergy(true)
    {
        cout << __func__ << '\t' << Plot<nYbins>::Title << endl;
        assert(yBins.size() >= static_cast<size_t>(nYbins));
        InitLine();
    }

    Plot<nYbins>& operator() (TGraph * g, const char * option, const char * loption, int y)
    {
        cd(y);

        g->Draw(option);

        TString title = g->GetTitle();
        if (title != "") l.at(y-1)->AddEntry(g, "", loption);

        return *this;
    }

    Plot<nYbins>& operator() (TF1 * f1, const char * option, const char * loption, int y)
    {
        cd(y);

        f1->Draw(option);

        TString title = f1->GetTitle();
        if (title != "") l.at(y-1)->AddEntry(f1, "", loption);

        return *this;
    }

    Plot<nYbins>& operator() (TH1 * h1, const char * option, const char * loption, int y)
    {
        cd(y);

        assert(h1 != nullptr);
        clean(h1, y);
        h1->Draw(option);

        TString title = h1->GetTitle();
        if (title != "") l.at(y-1)->AddEntry(h1, "", loption);

        return *this;
    }

    Plot<nYbins>& operator() (vector<TH1*> h1s, const char * option, const char * loption)
    {
        assert(h1s.size() == nYbins);

        for (int y = 1; y <= nYbins; ++y) {

            TH1 * h1 = h1s.at(y-1);
            assert(h1 != nullptr);
            operator()(h1, option, loption, y);
        }

        return *this;
    }

    Plot<nYbins>& operator() (THStack* hs, const char *option, int y)
    {
        cd(y);
        hs->Draw(option);

        return *this;
    }

    Plot<nYbins>& operator() (vector<vector<TH1*>> h1ss, const char *option, const char * loption)
    {
        assert(h1ss.size() == nYbins);

        for (int y = 1; y <= nYbins; ++y) {

            THStack * hs = new THStack(rn(), "");
            for (TH1 * h1: h1ss.at(y-1))  {
                clean(h1, y);
                hs->Add(h1);
                TString title = h1->GetTitle();
                if (title != "") l.at(y-1)->AddEntry(h1, "", loption);
            }
            operator()(hs, option, y);
        }

        return *this;
    }

    Plot<nYbins>& operator() (vector<TH2*> h2s, const char *option, const char * loption, vector<int> yLegends)
    {
        assert(h2s.size() == yLegends.size());
        vector<vector<TH1*>> h1ss(nYbins);
        size_t i = 0;
        for (TH2 * h2: h2s) {
            for (int y = 1; y <= nYbins; ++y) {
                TH1 * h1 = h2->ProjectionX(rn(), y, y);
                h1->SetTitle(y == yLegends.at(i) ? h2->GetTitle() : "");
                h1ss.at(y-1).push_back(h1);
            }
            ++i;
        }

        return operator()(h1ss, option, loption);
    }

    Plot<nYbins>& operator() (TH2* h2, const char *option, const char * loption, int yLegend = 1)
    {
        vector<TH1*> h1s;
        for (int y = 1; y <= nYbins; ++y) {
            TH1 * h1 = h2->ProjectionX(rn(), y, y);
            if (y == yLegend) h1->SetTitle(h2->GetTitle());
            else h1->SetTitle("");
            h1->SetFillColor(h2->GetFillColor());
            h1->SetMarkerColor(h2->GetMarkerColor());
            h1->SetLineColor(h2->GetLineColor());
            h1->SetFillStyle(h2->GetFillStyle());
            h1->SetMarkerStyle(h2->GetMarkerStyle());
            h1->SetLineStyle(h2->GetLineStyle());
            h1->SetMarkerSize(h2->GetMarkerSize());
            h1->SetLineWidth(h2->GetLineWidth());
            h1s.push_back(h1);
        }
        return operator()(h1s, option, loption);
    }

    ~Plot ()
    {
        cout << __func__ << '\t' << Plot<nYbins>::Title << endl;
    }

    //virtual Plot<nYbins>& DrawLineAtOne (float one = 1.) = 0;
    //virtual Plot<nYbins>& DrawVerticalLine (double v) = 0;
};
template<int nYbins> int Plot<nYbins>::kPosN = kPos2;
template<int nYbins> double Plot<nYbins>::shiftY = 1.5;
template<int nYbins> bool Plot<nYbins>::plotLeg = true;
template<int nYbins> bool Plot<nYbins>::plotHeader = true;
template<int nYbins> bool Plot<nYbins>::logx = true;
template<int nYbins> bool Plot<nYbins>::logy = false;
template<int nYbins> int Plot<nYbins>::ndivx = 404;
template<int nYbins> int Plot<nYbins>::ndivy = 606;

template<int nYbins> double Plot<nYbins>::RM = 0.01;
template<int nYbins> double Plot<nYbins>::LM = 0.08;

template<int nYbins = 5> struct NPlot1D : public Plot<nYbins> {

    vector<TCanvas *> c;
    
    void cd (int i) override
    {
        c.at(i-1)->cd();
    }

    void SetLogy () override
    {
        for (int y = 1; y <= nYbins; ++y) {
            cd(y);
            gPad->SetLogy();
        }
    }

private:
    void Init (const char * name, const char * title)
    {
        cout << __func__ << '\t' << title << endl;

        for (int y = 1; y <= nYbins; ++y) {

            c.push_back(new TCanvas(Form("%s_ybin%d", name, y), title, Plot<nYbins>::W, Plot<nYbins>::H));

            Plot<nYbins>::DrawFrame(y);

            SetFontsTicks(24, 10, -1);
            SetOffsets(1.7, 2.2, 0.5, 3.5);

            GetYaxis()->SetTitle(Plot<nYbins>::Ytitle); 
            GetXaxis()->SetTitle(Plot<nYbins>::Xtitle);
            GetXaxis()->SetNdivisions(NPlot1D<nYbins>::ndivx);
            GetYaxis()->SetNdivisions(NPlot1D<nYbins>::ndivy);
            if (NPlot1D<nYbins>::logx) {
                GetXaxis()->SetMoreLogLabels();
                GetXaxis()->SetNoExponent();
            }
            if (NPlot1D<nYbins>::logy) {
                GetYaxis()->SetMoreLogLabels();
                GetYaxis()->SetNoExponent();
            }
            gPad->SetTicks(1,1);
            gPad->RedrawAxis();

            if (y == 1) {
                Plot<nYbins>::l.at(y-1) = newLegend(Plot<nYbins>::kPosN);
                Plot<nYbins>::l.at(y-1)->SetNColumns(2);
                if (Plot<nYbins>::plotLeg) Plot<nYbins>::l.at(y-1)->AddEntry(new TObject, Form("Anti-k_{T} (R = 0.%d)", R), "h");
            }
            else
                Plot<nYbins>::l.at(y-1) = Plot<nYbins>::l.front();
        }
    }

public:
    NPlot1D (const char * name, const char * title,
             TString xtitle, TString ytitle, 
             double low, double high,
             vector<double> Min = vector<double>(MinDefault,nYbins),
             vector<double> Max = vector<double>(MaxDefault,nYbins),
             vector<TString> YBins = yBinsDefault) :
        Plot<nYbins>(name, title, 800, 600, xtitle, ytitle, low, high, Min, Max, YBins)
    {
        Init(name, title);
    }

    NPlot1D (const char * name, const char * title,
             TString xtitle, TString ytitle, 
             double low, double high,
             double Min = MinDefault,
             vector<double> Max = vector<double>(MaxDefault,nYbins),
             vector<TString> YBins = yBinsDefault) :
        Plot<nYbins>(name, title, 800, 600, xtitle, ytitle, low, high, vector<double>(nYbins,Min), Max, YBins)
    {
        Init(name, title);
    }

    NPlot1D (const char * name, const char * title,
             TString xtitle, TString ytitle, 
             double low, double high,
             double Min = MinDefault,
             double Max = MaxDefault,
             vector<TString> YBins = yBinsDefault) :
        Plot<nYbins>(name, title, 800, 600, xtitle, ytitle, low, high, vector<double>(nYbins,Min), vector<double>(nYbins,Max), YBins)
    {
        Init(name, title);
    }

    NPlot1D<nYbins>& DrawLineAtOne (float one = 1., Style_t style = kDashed)
    {
        for (int y = 1; y <= nYbins; ++y) {
            cd(y);
            Plot<nYbins>::line->SetLineStyle(style);
            Plot<nYbins>::line->DrawLine(Plot<nYbins>::x1.at(y-1), one, Plot<nYbins>::x2.at(y-1), one);
        }
        return *this;
    }

    NPlot1D<nYbins>& DrawVerticalLine (double v)
    {
        for (int y = 1; y <= nYbins; ++y) {
            cd(y);
            if (v < Plot<nYbins>::x1.at(y-1)) continue;
            if (v > Plot<nYbins>::x2.at(y-1)) continue;
            Plot<nYbins>::line->DrawLine(v, Plot<nYbins>::y1, v, Plot<nYbins>::y2);
        }
        return *this;
    }

    ~NPlot1D ()
    {
        cout << __func__ << '\t' << Plot<nYbins>::Title << endl;
        for (int y = 1; y <= nYbins; ++y) {
            cd(y);
            gPad->RedrawAxis();
            UpdateFrame();
            RemoveOverlaps(gPad, GetXaxis());
            DrawLatexUp(-Plot<nYbins>::shiftY, Plot<nYbins>::yBins.at(y-1));
            if (Plot<nYbins>::plotLeg) DrawLegends({(TLegend*)Plot<nYbins>::l.at(y-1)->Clone(rn())},true/*, Plot<nYbins>::legAlpha*/);

            if (Plot<nYbins>::plotHeader)
                DrawLatexUp(0.8, CMSheader, 32, "l");
            if (Plot<nYbins>::plotEnergy) {
                if (Plot<nYbins>::isMConly || !Plot<nYbins>::plotLumi) DrawLatexUp(1., "13 TeV", 22, "r");
                else                                                   DrawLatexUp(1., Form("%.1f fb^{-1} (13 TeV)", L), 22, "r"); // TODO
            }

            if (CMSheader.Contains("Internal")) {
                TDatime * datime = new TDatime;
                static TString now = Form("Figure produced on %d-%d-%d %d:%d:%d", datime->GetYear(), datime->GetMonth(), datime->GetDay(), datime->GetHour(), datime->GetMinute(), datime->GetSecond());
                cout << now << endl;
                DrawLatexDown(2.5, now, 22, "l");
            }

            const char suffix = nYbins > 1 ? (y == 1 ? '(' : y == nYbins ? ')' : '\0') : '\0';
            c.at(y-1)->SaveAs(Form("%s_1D.pdf%c", Plot<nYbins>::Name.Data(), suffix),
                  Form("Title:%s", Plot<nYbins>::Title.Data()));
            delete c.at(y-1);
        }
    }

};

template<int nYbins = 5> struct PlotND : public Plot<nYbins> {

    static char suffix;

    constexpr static const double TM = 32, BM = 46;

    const int nRows;
    const int d;

    TCanvas * c;

    void cd (int i) override
    {
        if (i == 0) c->cd();
        else        c->cd(i);
    }

    void SetLogy () override
    {
        for (int row = 0; row < nRows; ++row)
        for (int y = 1; y <= nYbins; ++y) {
            cd(row*nYbins+y);
            gPad->SetLogy();
        }
    }

    template<typename ...Args>
    PlotND (int Nrows,
          Args... args) :
        Plot<nYbins>(args...),
        nRows(Nrows), d(nRows > 1 ? 3 : 2),
        c(new TCanvas(Plot<nYbins>::Name, Plot<nYbins>::Title, Plot<nYbins>::W, Plot<nYbins>::H))
    {
        cout << __func__ << '\t' << Plot<nYbins>::Title << endl;
        c->SetTopMargin(TM/Plot<nYbins>::H);
        c->SetRightMargin(Plot<nYbins>::RM);
        c->SetLeftMargin (Plot<nYbins>::LM);
        c->SetBottomMargin(BM/Plot<nYbins>::H);

        DivideTransparent(group(1,0,nYbins),group(1,0,nRows));

        for (int row = 0; row < nRows; ++row)
        for (int y = 1; y <= nYbins; ++y) {
            cd(row*nYbins+y);

            Plot<nYbins>::DrawFrame(y);
            SetFontsTicks(16, 4, -1);
            SetOffsets(1.6, 2.4, 0.5, 4);

            if (row == 0 && y == 1) GetYaxis()->SetTitle(Plot<nYbins>::Ytitle); 
            if (y > 1) GetYaxis()->SetLabelSize(0);
            if (row == nRows - 1 && y == nYbins) GetXaxis()->SetTitle(Plot<nYbins>::Xtitle);
            if (row < nRows - 1) GetXaxis()->SetLabelSize(0);

            GetXaxis()->SetNdivisions(PlotND<nYbins>::ndivx);
            GetYaxis()->SetNdivisions(PlotND<nYbins>::ndivy);
            if (PlotND<nYbins>::logx) {
                GetXaxis()->SetMoreLogLabels();
                GetXaxis()->SetNoExponent();
            }
            if (PlotND<nYbins>::logy) {
                GetYaxis()->SetMoreLogLabels();
                GetYaxis()->SetNoExponent();
            }
            GetZaxis()->SetLabelSize(PxFontToRel(16));
            GetZaxis()->SetTitleSize(PxFontToRel(16));
            gPad->SetTicks(1,1);
            gPad->RedrawAxis();

            if (row > 0) continue;
            Plot<nYbins>::l.at(y-1) = newLegend(Plot<nYbins>::kPosN);
            if (Plot<nYbins>::plotLeg && y == 1 && Plot<nYbins>::plotHeader)
                Plot<nYbins>::l.at(y-1)->AddEntry(new TObject, Form("Anti-k_{T} (R = 0.%d)", R), "h");
        }
    }

    PlotND<nYbins>& DrawLineAtOne (float one = 1., Style_t style = kDashed)
    {
        for (int row = 0; row < nRows; ++row)
        for (int y = 1; y <= nYbins; ++y) {
            cd(row*nYbins+y);
            Plot<nYbins>::line->SetLineStyle(style);
            Plot<nYbins>::line->DrawLine(Plot<nYbins>::x1.at(y-1), one, Plot<nYbins>::x2.at(y-1), one);
        }
        return *this;
    }

    PlotND<nYbins>& DrawVerticalLine (double v)
    {
        for (int row = 0; row < nRows; ++row)
        for (int y = 1; y <= nYbins; ++y) {
            cd(row*nYbins+y);
            if (v < Plot<nYbins>::x1.at(y-1)) continue;
            if (v > Plot<nYbins>::x2.at(y-1)) continue;
            Plot<nYbins>::line->DrawLine(v, Plot<nYbins>::y1, v, Plot<nYbins>::y2);
        }
        return *this;
    }

    ~PlotND ()
    {
        cout << __func__ << '\t' << Plot<nYbins>::Title << endl;
        c->cd(1);
        cout << __LINE__ << endl;
        if (Plot<nYbins>::plotHeader)
            DrawLatexUp(0.8, CMSheader, 27, "l");
        cout << __LINE__ << endl;
        c->cd(nYbins);
        cout << __LINE__ << endl;
        if (Plot<nYbins>::plotEnergy) {
            if (Plot<nYbins>::isMConly || !Plot<nYbins>::plotLumi) DrawLatexUp(1., "13 TeV", 22, "r");
            else                                                   DrawLatexUp(1., Form("%.1f fb^{-1} (13 TeV)", L), 22, "r"); // TODO?
        }

        for (int row = 0; row < nRows; ++row)
        for (int y = 1; y <= nYbins; ++y) {
            cd(row*nYbins+y);
            gPad->RedrawAxis();
            UpdateFrame();
            RemoveOverlaps(gPad, GetXaxis());
            if (row > 0) continue;
            DrawLatexUp(-Plot<nYbins>::shiftY, Plot<nYbins>::yBins.at(y-1));
            if (Plot<nYbins>::plotLeg) {
                DrawLegends({Plot<nYbins>::l.at(y-1)},true/*, Plot<nYbins>::legAlpha*/);
            }
        }

        cd(0);
        if (CMSheader.Contains("Internal")) {
            TDatime * datime = new TDatime;
            TString now = Form("Figure produced on %d-%d-%d %d:%d:%d", datime->GetYear(), datime->GetMonth(), datime->GetDay(), datime->GetHour(), datime->GetMinute(), datime->GetSecond());
            cout << now << endl;
            DrawLatexDown(2.45, now, 18, "l");
        }

        c->SaveAs(Form("%s_%dD.pdf%c", Plot<nYbins>::Name.Data(), d, PlotND<nYbins>::suffix),
                  Form("Title:%s", Plot<nYbins>::Title.Data()));
        delete c;
    }
};
template<int nYbins> char PlotND<nYbins>::suffix = '\0';

template<int nYbins = 5> struct Plot2D : public PlotND<nYbins> {

    Plot2D (const char * name, const char * title,
            const char * xtitle, const char * ytitle,
            double low, double high, double X1 = MinDefault, double X2 = MaxDefault,
            vector<TString> YBins = yBinsDefault) :
        PlotND<nYbins>(1, name, title, 800, 300, xtitle, ytitle, low, high, X1, X2, YBins)
    { }

    Plot2D (const char * name, const char * title,
            const char * xtitle, const char * ytitle, 
            double low, double high,
            double X1 = MinDefault,
            vector<double> Max = vector<double>(MaxDefault,nYbins),
            vector<TString> YBins = yBinsDefault) :
        PlotND<nYbins>(1, name, title, 800, 300, xtitle, ytitle, low, high, X1, Max, YBins)
    { }

    Plot2D (const char * name, const char * title,
            const char * xtitle, const char * ytitle, 
            double low, double high,
            vector<double> Min = vector<double>(MinDefault,nYbins),
            vector<double> Max = vector<double>(MaxDefault,nYbins),
            vector<TString> YBins = yBinsDefault) :
        PlotND<nYbins>(1, name, title, 800, 300, xtitle, ytitle, low, high, Min, Max, YBins)
    { }
};

template<int nYbins = 5> struct Plot3D : public PlotND<nYbins> {

    Plot3D (const char * name, const char * title,
            const char * xtitle, const char * ytitle,
            double low, double high, double X1 = 97, double X2 = 548,
            vector<TString> YBins = yBinsDefault, int nRows = 2) :
        PlotND<nYbins>(nRows, name, title, 800, nRows*(300-PlotND<nYbins>::TM-PlotND<nYbins>::BM), xtitle, ytitle, low, high, X1, X2, YBins)
    { }

    void cd (int row /* starting from 0 */, int y /* starting from 1 */)
    {
        PlotND<nYbins>::cd(row*nYbins + y);
    }

    Plot3D<nYbins>& operator()(vector<vector<TH1*>> h1s, const char * option, const char * loption, int yLegend = 1)
    {
        assert(h1s.size() == PlotND<nYbins>::nRows);
        for (auto v: h1s)
            assert(v.size() == nYbins);

        TString title = h1s.front().front()->GetTitle();

        cout << title << endl;
        for (int row = 0; row < PlotND<nYbins>::nRows; ++row)
        for (int y = 1; y <= nYbins; ++y) {

            PlotND<nYbins>::cd(row*nYbins + y);

            TH1 * h1 = h1s.at(row).at(y-1);
            assert(h1 != nullptr);
            PlotND<nYbins>::clean(h1, y);
            h1->Draw(option);

            if (row == 0 && y == yLegend && title != "")
                PlotND<nYbins>::l.at(yLegend-1)->AddEntry(h1, "", loption);
        }

        return *this;
    }

    Plot3D<nYbins>& operator()(TH3* h3, const char *option, const char * loption, int yLegend = 1)
    {
        vector<vector<TH1*>> h1s(PlotND<nYbins>::nRows, vector<TH1*>(nYbins, nullptr));
        for (int row = 1; row <= PlotND<nYbins>::nRows; ++row)
        for (int y = 1; y <= nYbins; ++y) {
            TH1 * h1 = h3->ProjectionX(rn(), y, y, row, row);
            h1->SetTitle(h3->GetTitle());
            h1->SetFillColor(h3->GetFillColor());
            h1->SetMarkerColor(h3->GetMarkerColor());
            h1->SetLineColor(h3->GetLineColor());
            h1->SetFillStyle(h3->GetFillStyle());
            h1->SetMarkerStyle(h3->GetMarkerStyle());
            h1->SetLineStyle(h3->GetLineStyle());
            h1->SetMarkerSize(h3->GetMarkerSize());
            h1->SetLineWidth(h3->GetLineWidth());
            h1s.at(row-1).at(y-1) = h1;
        }
        return operator()(h1s, option, loption, yLegend);
    }
};

#endif
